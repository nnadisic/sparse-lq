function [alpha,Wtot] = checkNNIndepUntilOrder4(W, sparsity)
% alpha = checkNNIndepUntilOrder4(W)
% Enables to check whether the columns of W are nonnegatively independent
% with a decomposition in Delta.

cb = nchoosek(1:size(W,2),2);
WoW = W(:,cb(:,1)).*W(:,cb(:,2));

cb = nchoosek(1:size(W,2),3);
WoWoW = W(:,cb(:,1)).*W(:,cb(:,2)).*W(:,cb(:,3));

cb = nchoosek(1:size(W,2),4);
WoWoWoW = W(:,cb(:,1)).*W(:,cb(:,2)).*W(:,cb(:,3)).*W(:,cb(:,4));

Wtot = [W,WoW,WoWoW,WoWoWoW];

[alpha,~] = alphaparam_delta(Wtot,sparsity);

end