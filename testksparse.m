clear all; clc;

% datafile = "Donnees/data_indNbSpectres1_it_MC1.mat";
% load(datafile)
% ksparsity = 2;
% [m, n] = size(M);
% [~, r] = size(W0);

nbMC = 100; % number of monte carlo per parameter

% ranks = [2,3,4,6,8,10];
ranks = [2,3,4,6];
nbranks = length(ranks);
% sparsities = [1 2];
sparsities = [2];

% results = ones(nbMC,nbranks,length(sparsities));
results = ones(nbMC,nbranks);
for i = 1:length(ranks)
    r = ranks(i);
    for j = 1:nbMC
        for k = sparsities
            datafile = sprintf("Donnees/data_indNbSpectres%i_it_MC%i.mat",i,j);
            display(datafile)
            load(datafile)
            [alpha, ~] = checkNNIndepUntilOrder4(W0(:,1:r),k);
            results(j,i) = alpha;
        end
    end
end

% load('Results.mat', 'alphaTab');
plot(ranks,mean(alphaTab(:,1:nbranks)),'b');
hold on;
plot(ranks,mean(results(1:100,1:nbranks)),'g');
% plot(ranks,mean(results(:,1:nbranks,1)),'r');
% plot(ranks,mean(results(:,1:nbranks,2)),'g');
hold off;
