function H = matactiveset(M,W)
[~, n] = size(M);
[~, r] = size(W);

H = zeros(r, n);

AtA = W'*W;

for i = 1:n
    Atb = W'*M(:,i);
    H(:,i) = activesetsumtoone(AtA, Atb);
end


end

