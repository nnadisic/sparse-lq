% Params
m=10 ; n=20 ; r=5 ;

% Generate H zhere cols sum to one
realH = rand(r,n);
% D = spdiags(((sum(realH)+1e-16).^(-1))', 0, n, n);
% realH = realH*D;
sum(realH)

% Generate W and M
realW = rand(m,r);
realM = realW*realH;

% Compute
myH = bruteksparse(realM, realW, 5);
sum(myH)