function H = bruteksparse(M, W, k)
%BRUTEKSPARSE given M and W, return H col-wise k-sparse minimizing
% ||M-WH||_F^2

[~, n] = size(M);
[~, r] = size(W);

H = zeros(r, n);

% AtA = W'*W;

% Loop on columns of H
for i = 1:n
%     Atb = W'*M(:,i);
    % Try all k-sparse supports
    allsupp = make_perm(r,k,1);
    besth = zeros(r,1);
    besterr = Inf;
    for s=1:size(allsupp,1)
        supp = allsupp(s,:);
        h = zeros(r,1);
        h(supp) = FGMfcnls(M(:,i), W(:,supp), [], 100, 1e-4);
        err = norm(W*h - M(:,i));
        if err < besterr
            besterr = err;
            besth = h;
        end
    end
    H(:,i) = besth;
end

end

