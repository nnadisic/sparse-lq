function x=activesetsumtoone(AtA,Atb)
    [~,n] = size(AtA);
    F = false(n,1); %F: passive set, càd ceux qui ne sont pas imposés à 0, ~F: active set, càd ceux qui sont imposés à 0
    
%     AtA=A'*A;
%     Atb=A'*b;
    
    t = n+1;
    p = 3;
    
    x = zeros(n,1); %solution
    y = zeros(n,1); %gradient
    
    %résoudre le problème aux moindres carrés
    sF=sum(F); %taille du passive set
    As = [AtA(F,F) -ones(sF,1) ; ones(1,sF) 0];
    bs = [Atb(F) ; 1];
    sol = As\bs;
    x(F) = sol(1:sF);
    mu= sol(sF+1);
    x(~F) = 0;
    y(~F) = AtA(~F,F)*x(F)-Atb(~F)-mu;  %calcul du gradient
    y(F) = 0;
    
    H1 = x<0 & F;  %F(x(F)<0);
    H2 = y<0 & ~F; %~F(y(~F)<0);
    
    notgood = sum(H1) + sum(H2); %H1unionH2 = unique([H1 H2]);
    
    while notgood>0
    
        if notgood<t
            t   = notgood;
            p   = 3;
            H1c = H1;
            H2c = H2;
        end

        if notgood>=t && p>=1
            p   = p-1;
            H1c = H1;
            H2c = H2;
        end

        if notgood>=t && p==0
            r = find(H1&H2,1,'last'); %r = max(H1unionH2);
            if H1(r) %~isempty(intersect(H1,r))
                H1c = false(n,1); H1c(r)=true;
                H2c = false(n,1);
            else
                H1c = false(n,1);
                H2c = false(n,1); H2c(r)=true;
            end
        end
        F = (xor(F,H1c))|H2c; %F = unique([setdiff(F,H1c) H2c]);
        
        sF=sum(F);
        As = [AtA(F,F) -ones(sF,1) ; ones(1,sF) 0];
        bs = [Atb(F) ; 1];
        sol = As\bs;
        x(F) = sol(1:sF);
        mu= sol(sF+1);
        x(~F) = 0;
        y(~F) = AtA(~F,F)*x(F)-Atb(~F)-mu;  %calcul du gradient
        y(F)  = 0;

        H1 = x<0 & F;  %F(x(F)<0);
        H2 = y<0 & ~F; %~F(y(~F)<0);

        notgood = sum(H1) + sum(H2); %H1unionH2 = unique([H1 H2]);
    end
end
