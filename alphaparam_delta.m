% Given an m-by-r matrix W, comptues:
%
% alpha = min_i min_{x >= 0} ||W(:,i) - W(:,I) x||_2 / ||W(:,i)||_2
% where I = {1,2,...,r}\{i}
%
% and k = index for which the minimum is achieved.

function [alpha,k] = alphaparam_delta(W,sparsity)

[~,r] = size(W);

alpha = +Inf;


% Looping over the column of W
for i = 1 : r
    if alpha > 1e-6 % otherwise, alpha = 0 up to numerical errors
        %x = arborescentMex(W(:,i),W(:,[1:i-1 i+1:r]),sparsity);
        %x = k_s_nmf(W(:,i), W(:,[1:i-1 i+1:r]), sparsity);
        x = bruteksparse(W(:,i), W(:,[1:i-1 i+1:r]), sparsity);
        alphai =  norm(W(:,i) - W(:,[1:i-1 i+1:r])*x );
        if alphai < alpha
            alpha = alphai;
            k = i;
        end
    end    
end